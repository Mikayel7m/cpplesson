#include <iostream>
#include <string>

class Polygon {
private:
    int m_side;
    std::string  m_color;
public:
    int virtual area() = 0;
    
    // Polygon(int side ,std::string color){
    //    side = m_side;
    //    color = m_color;
    
    void setSide(int side){
      m_side = side;  
    }   
   // int getSide() const {
   //     return m_side;
   // }
    
    void setColor(std::string color) {
        m_color = color;
    }
    
    std::string getColor(){
        return m_color;
    }

    int getArea(){
        return  m_side * m_side;
    } 

};

class Rectangle{
private:
    int height;
    int width;
public:
   void  setHeight(int side ){
        height = side;
   }

    void setWidth(int side){
        width = side; 
    }  
 
   int getArea(){
        return width * height;
    }
};


int main(){
    Polygon* a;
    a->setSide(15);
    a->getArea();
    std::cout<< a->getArea()<<"  \n";
    a->setColor("Red");
    std::cout<<a->getColor()<<"\n";

    return 0;
}
