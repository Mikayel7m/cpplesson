#include <iostream>

int add(int a, int b) {
    return a + b ;
} 

int sub(int a , int b) {
    return a - b ;
}

int mult(int a , int b) {
    return a * b ;
}

int main() {
 
    //  int(*func[3])(int ,int);
    // int r ;
    // func = add; 
    // r = func(12,15);
    // std::cout <<r<<"\n";
   // func = sub;
   //  r = func(55,2);
    // std::cout <<r<<"\n";
    // func = mult;
   // r = func(3,2);
   // std::cout<<r<<"\n";
   // int arr[] = {add,sub,mult};
    int(*p[3])(int,int) = {add,sub,mult};
    for (int i = 0 ; i < 3 ; ++i) { 
           int  r = p[i](5,5);
            
             
             std::cout<<r<<"\n";
    }   

        return 0;
}
