#include <string.h>
#include <iostream>
class Foo {
    private:
        int * m_p;

    public:
        Foo(int i ){
            m_p = new int(i);
        }
        
        ~Foo(){
            std::cout<< "ByBY \n";
            delete m_p;
        }
       
        Foo(const Foo &obj){
            m_p = new int(* obj.m_p);
        }
        
        void operator=(const Foo &obj) {
            *m_p = *obj.m_p;
        }
        void print(){
            std::cout<<*m_p<<"\n";
            std::cout<<"Address  "<<m_p<<"\n";
        }
};

void f(Foo f1) {
    f1.print();
}

int main(){
    Foo f1(2);-/*Copy
    Foo f2 = f1; */
  
    Foo f1(2);-/*veragrum
    Foo f2(2);
    f2 = f1; */
    
     f(f1);
}
