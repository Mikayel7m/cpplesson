#include <iostream>

class SomeClass {
    int *ptr;
public:
    SomeClass(){
        std::cout<<"\nSimple Constructor \n";
    }
    SomeClass(const SomeClass &obj) {
        std::cout<<"\nCopy Constructor \n";
    }
    ~SomeClass(){
         std::cout<<"\n Destructor \n";
    }
};
void funcShow(SomeClass object){
    std::cout<<"\nFunction get object as parameter\n";
}
SomeClass funcReturnobject(){
    SomeClass object;
    std::cout<<"\nFunction return object\n";
    return object;
}

int main(){
    std::cout<<"\n1_____________________________\n";
    SomeClass obj1;
    std::cout<<"\n2_____________________________\n";
    funcShow(obj1);
    std::cout<<"\n3-4___________________________\n";
    funcReturnobject();
    std::cout<<"\nr5`___________________________\n";
    SomeClass obj2 = obj1;

    return 0;
} 
