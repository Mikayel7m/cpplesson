//#include <iostream>
//
//class MyClass {
//private:
//   int a, b;
//public:
//   MyClass(int i , int j){
//    a = i;
//    b = j;
//   }
//   friend int sum(MyClass x);
//};
//
//class B:public MyClass {
//    
//}
//
//int sum(B x) {
//    return x.a + x.b;
//}
//
//int main(){
//    
//    MyClass n(3,4);
//    B d(3,9);
//    std::cout<<sum(n)<<sum(d) ;
//
//    return 0;
//}


#include <iostream>
const int IDLE = 0;
const int INUSE = 1;

class C2;
class C1 {
private:
    int status;
public:
    void set_status(int state);
    friend int idle(C1 a, C2 b);
};

class C2 {
private:
    int status;
public:
    void set_status(int state);
   /* friend*/ int idle(C2 b);
};

void C1::set_status(int state){
    status = state;
}

void C2::set_status(int state){
    status = state;
}

int C1::idle(C2 b) {
    if(status || b.status) return 0;
    else return 1;
}


//int idle(C1 a ,C2 b) {
//    if(a.status || b.status) return 0;
//    else return 1;
//}

int main () {
    C1 x ;
    C2 y ;
    x.set_status(IDLE);
    y.set_status(IDLE);
    
    if(x.idle(y)) {
        std::cout<<"Empty Display \n";
     } else {
         std::cout<<" Display \n";
     }
     x.set_status(INUSE);

     if(x.idle(y)) {
        std::cout<<"Empty Display \n";
     } else {
         std::cout<<" Display \n";
     }
    return 0;
}
