#include <iostream> 
#include <cstring>
#include <cstdlib>

class sample {
private:
    char *s;
public:
    sample() { s = 0; }
    ~sample(){
        if(s) {
            delete [] s;
            std::cout<<"empty S \n";    
        }
    }
    void show() {
       std::cout<<s<<"\n";
    }
    void set(char *str);
};

void sample::set(char *str){
    s = new char[strlen(str) + 1];
    strcpy(s,str);
}
sample input(){
    char instr[80];
    sample str;
    std::cout<<"Enter Text";
    std::cin>>instr;
    str.set(instr);
    return str;
}

int main(){
    sample ob;

    ob = input();
    ob.show();

    return 0;
}
