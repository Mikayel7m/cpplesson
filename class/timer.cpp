#include <iostream>
#include<cstdlib>
#include<ctime>

class timer{
private:
    int seconds;
public:
    timer(char* t){
        seconds = atoi(t);
    }
    timer(int t){
        seconds = t;
    }
    timer(int min, int sec){
        seconds = min *60 + sec;
    
    }
    void run();
};
void timer::run(){
    clock_t t1;
    t1 = clock();

    while((clock()/CLOCKS_PER_SEC - t1/CLOCKS_PER_SEC) < seconds);

    std::cout<<"\a";
}
int main(){
       timer a(10);
       a.run();
       std::cout<<"Enter seconds: ";
       char str[80];
       std::cin >>str;
       timer b(str);
       b.run();
    
       std::cout<<"Enter minutes and seconds: ";
       int min,sec;
       std::cin>>min>>sec;
       timer c(min, sec);
       c.run();

        return 0;
} 

