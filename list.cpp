#include <iostream>
#include <cstdlib>
#include <cassert>

struct Node{
int data;
Node* next;
};



//declarat functions
Node* creatList(int base = 0);
Node* addelem(Node*,int);
Node* instertElem(Node*,Node*,int );
Node* deletList(Node*);
Node* deletByElem(Node*,Node*);
Node* getElem(Node*,int);
void printList(Node *);


int main()
{
	Node* start;
	start = creatList(8);
	start = addelem(start,10);
	start = addelem(start,11);
	start = addelem(start,12);
	start = addelem(start,13);
	printList(start);
	Node* l = start->next;
	deletByElem(start, l);	
	printList(start);
	start = instertElem(start,start->next->next, 44);
	printList(start);	
	std::cout << getElem(start, 3)->data << std::endl;
	start = deletList(start);
	printList(start);	
//	printList(start);	std::cout << getElem(start, 4)->data << std::endl;
	return 0;
}


//creat functions
Node* creatList(int dest)
{
	Node* list = (Node*)malloc(sizeof(Node));
	list -> data = dest;
	list -> next = NULL;
	return list;
}


Node* addelem(Node* list,int data)
{
	if(list == NULL){
		return creatList();
	}
	Node* env = list;
	while(env -> next != NULL){
		env = env -> next;
	}
	Node* var = (Node*)malloc(sizeof(Node));
	var -> data = data;
	var -> next = NULL;
	env -> next = var;
	return list;
}

Node* instertElem(Node* list, Node* it, int data)
{
	assert(list != NULL);
	assert(it != NULL);
	Node * p = list;
	while(p != it && p != NULL) {
		p = p->next;
	}
	assert(p != NULL); 
	Node* var = (Node*)malloc(sizeof(Node));
	var -> data = data;
	var -> next = it -> next; 
	it -> next = var;
	return list;
}

Node* deletList(Node* list)
{
	Node* var = list;
	while(var != NULL){
		Node* p = var;
		var = var->next;
		free(p);
	}
	return NULL;
}

Node* deletByElem(Node* list, Node* index)
{
	assert(index != NULL);
	assert(list != NULL);
	Node* p = list;
	if (list == index) {
		Node * p = list->next;
		free(list);	
		return p;
	}
	while(p->next != index && p != NULL) {
		p = p->next;
	}
	assert(p != NULL); 
	p->next	= index->next;
	free(index);
	return list;

}
Node* getElem(Node* list,int index)
{
	assert(list != NULL);
	Node *p = list;
	for (int i = 0; i < index; ++i) {
		p = p->next;
	}
	assert(p != NULL);
	
	return p;
}

void printList(Node* l)
{
	std::cout << "----------------START--------------" << std::endl;
	while(l != NULL) {
		std::cout << l->data << std::endl;
		l = l->next;
	}
	std::cout << "-----------------END---------------" << std::endl;
}
