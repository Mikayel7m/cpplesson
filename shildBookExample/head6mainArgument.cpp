#include <iostream>

void swap(int* i, int*j) {
    int tmp = *i;
    *i = *j;
    *j = tmp; 
}

void swap(int &a,int &b){
    int tmp = a;
    a = b;
    b = tmp;
}
int main(int argc, char *argv[]){
//if (argc !=2) {
//    std::cout<<"You are not write name \n";
//    return 1;
//}
//    std::cout<<argv[2]<<"\n";

    int *i;
    int* j;
    int a = 5;
    int b = 12;
    i = &a;
    j = &b;
    int &g = a;
    int &h = b;
    swap(h,g);
    std::cout<<a<<' '<< b;
    return 0;
}
