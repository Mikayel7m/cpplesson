#include<iostream>

class Queue {
private:
    int q[100];
    int sloc,rloc;
    int who;

public:
   // void init();
    Queue();
    Queue(int id);
    ~Queue();
    void qput(int i);
    int qget();
};

Queue::Queue(int id){
    sloc = rloc = 0;
    who = id;
}

Queue::Queue(){
    std::cout<<"Constructor \n";
    sloc = rloc  = 0;
}

Queue::~Queue(){
    std::cout<<"Destructor \n";
}
//void Queue::init(){
//    rloc = sloc = 0;
//}

void Queue::qput(int i){
    if (sloc ==100){
        
            std::cout<<"error \n";
    }
sloc++;
q[sloc] = i;
}

int Queue::qget(){
    if(rloc == sloc) {
        std::cout<<"Queue is Empty.\n";
        return 0;
    }
    rloc++;
    return q[rloc];
}

int main(){
    Queue a,b;
//    a.init();
//    b.init();

    a.qput(10);
    b.qput(19);

    a.qput(20);
    b.qput(1);

    std::cout<<"a is :: "<<a.qget()<<" "<<a.qget()<<"\n";
    std::cout<<"b is :: "<<b.qget()<<" "<<b.qget()<<"\n";
    return 0;
}
