#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
typedef struct Node node;

struct Node
{
	int data;
	node * prev;
}; 
	
node * createStack(int data);
node * pushStack(node* stack, int data);
int topStack(node* stack);
node * popStack(node* stack);
void deleteStack(node* stack); // or return node* with value NULL;

int main() {
    node * new = createStack(10);
    printf("data = %d \n",new->data);
    node *p1 = pushStack(new,20);
    //node *p2 = pushStack(p1,30);
    printf("data: %d prev:%d \n",p1->data,p1->prev->data);
  //  printf("data: %d prev:%d \n",p2->data,p2->prev->data);
    node *pop = popStack(p1);
    printf("data: %d prev:%d \n",p1->data ,*p1->prev);
    int tp = topStack(p1); 
    printf("ddd %d \n", tp);
    deleteStack(pop);
        
    return 0;
}

node * createStack(int data){
    node * newData =(node*)malloc(sizeof(node));
    newData->data = data;
    newData->prev = NULL;
        
    return newData;
}

node * pushStack(node* stack , int data){  
    node * head = (node*)malloc(sizeof(node*));
    if (head == NULL) {
        exit(0);
    }
    head->data = data;
    head->prev = stack;
    stack = head;
    return stack;
}

node * popStack(node* stack){
    node* head = stack;
    if (stack != NULL) {
       stack = stack->prev;
        free(head);
    }
    return stack;
}

int topStack(node* stack){
   // node* head = stack;
   // int headData;
   // if (head != NULL){
   //      do{
   //         printf("data %d \n",head->data);
   //     headData = head->data ;
   //      break;
   //         head = head->prev;
   //      }while(head !=NULL);

   // }
   // return headData;
    return (stack->data);
}
void deleteStack(node* stack){
   node* temp = stack;
    if (stack != NULL){
        while(stack->data !=NULL){
            node * p = temp;
            temp =temp->prev;
            free(p); 
        }
   }
printf("%d",stack->data);
return;
}   
