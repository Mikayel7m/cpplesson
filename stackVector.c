#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>

typedef struct Vector{
    void * array;
    int arraySize;
    int elemSize;
    int allocLength;
}vector;

vector * createVector(int);
int isEmpty(vector *);
vector * push( vector *,void *);
vector *  pop(vector *, void *);
void delete(vector*); 

int main(){

    vector  *create = createVector(10);
    vector *p = push(create,5);
    vector *p1 = push(create,6);
    vector *p2 = push(create,7);
    vector *p3 = push(create,8);
    vector *p4 = push(create,9);
    vector *p5 = push(create,10);
    int i = 0;
    while(isEmpty == 0){
        printf("%d", create->array[i]);
        i ++;
    }
    
    return 0;
}

vector * createVector(int elemSize){
    vector * vec = malloc(sizeof(vector));
    vec->arraySize = 0;
    vec->allocLength = 2;
    vec->elemSize = elemSize;
    vec->array = malloc(vec->arraySize * vec->allocLength);
    return vec;    
}

int isEmpty(vector * vec) {
    assert(vec !=NULL);
    if (vec->arraySize !=0){
        return 0;
    }else {
        return 1;
    }
}

vector * push (vector * v,void * data) {
    assert (v != NULL);
    if (v->arraySize == v->allocLength){
        v->allocLength *= 2;
        v->array = realloc(v->array,v->elemSize * v->allocLength);
    }   
    v->arraySize ++;
    return v;
}

vector * pop(vector * vec, void *data){
    assert(vec != NULL);
    memcpy(data,(char*)(vec->array +(vec->arraySize -1) * vec->elemSize) , vec->elemSize);
    vec->array --;
    return vec;   

}
void delete(vector * vec){
    if(vec !=NULL){
        if (vec->array != NULL){
            free(vec->array);
        }
        free(vec);
    }
}
