#include <iostream>
#include <cstdio>
#include <cstring>
#include <cstdlib>

void swap(void*a,void*b,int size );

int intCmp(void *p1,void *p2) {
    return  *(int*)p1 - *(int*)p2;
}

int lsearch(int key,int*arr,int arrSize);

void * genSearch(void* key , void *base, int n, int elemSize, int
        (*cmpfn)(void*cp1,void* vp2)) {

for (int i = 0; i <n ; i++) {
    void* vp = (char*)base +i *elemSize; 
    if(cmpfn(key ,vp) == 0 ) {
    
        return vp;

    }
}
return NULL;

}

int main() {
     // int *cmp(int* , int*);
      //int r = intCmp();
      int arr[5] = {58,9,6,2,1};
      int a = 99;  
      int *p = (int *)genSearch(&a,arr,5,sizeof(int),intCmp);
      std::cout<<*p<<"\n";
      // char * str1 = "Hello";
   // char * str2 = "vvvdsd";
   // swap(&str2,&str1,sizeof(char*));
   // std::cout<<str1<<"\n";
   // std::cout<<str2<<"\n";
   // int arr[] = {12,56,7,98,5,20};
   // int t = lsearch(7,arr,6);
   // std::cout<<t<<"\n";
   // return 0;


    return 0;

}

int lsearch(int key,int*arr,int arrSize) {
    int i;
    for (i =0 ; i < arrSize;++i) {
        if (key == *(arr +i)){
        return i;
        }
    
    }
}


void swap(void*a,void*b,int size) {
    char *tmp[size];
    memcpy(tmp,a,size);
     memcpy(a,b,size);
    memcpy(b,tmp,size);

    return;
}
