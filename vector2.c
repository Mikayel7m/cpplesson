#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

struct vector {
	int * arr;
	int size;
	int allocLength;
	int elemSize;
};

typedef struct  vector vector;

vector * createVector(int ,int );
void push (vector*,void*);
void getElementByIndex(vector * v, void * data, int index);
int getSize(vector*);
int getAllocLength(vector * );
int getIndexOfElem(vector *,int);
void delete(vector *);
void deleteElementByIndex(vector * ,int);
int pop(vector *);
int main(){
    vector * createVec = createVector(10,10);
    
    
    return 0;
}

vector * createVector(int size, int elemSize){
	vector * v = (vector*)malloc(sizeof(vector));
	v->size = 0;
	v->allocLength = 2;
	v->elemSize = elemSize;
	 v->arr=malloc(size * v->allocLength);
//	v->arr* = (int*)malloc(sizeof(int)) * (v->allocLength);
	return v;
}

void push (vector * v,void * data)
{
	assert (v != NULL);
	if (v->size == v->allocLength){
		v->allocLength *= 2;
		v->arr = realloc(v->arr,v->elemSize * v->allocLength);
    }
		memcpy((char*)v->arr + v->size * v->elemSize,data,v->elemSize);
		//*(v->arr + size) = date;
		v->size ++;
}

void getElementByIndex(vector * v, void * data, int index)
{
	assert (v != NULL);
	assert (index < v->size);
	assert (index >= 0);
	memcpy(data,(char*)(v->arr + index * v->elemSize),v->elemSize);
	return ; /* *(v->arr = index);*/
}

int getSize(vector * v) {
	assert(v == NULL);
	return v->size;
}

int getAllocLength(vector * v){
	assert (v != NULL);
	return v-> allocLength;

int getIndexOfElem(vector * v,void* data){
	assert(v != NULL);
	for (int i = 0;i < v->elemSize;i++){
	if (memcmp((char*)(v->arr + i *
             v->elemSize),data,v->elemSize)==0);
		return i;
		}
	}
	return -1;
}


void delete(vector * v){
	if (v != NULL);
	if (v->arr=NULL);{
		free(v->arr);
	}
	free (v);
}

void  deleteElementByIndex(vector * v,int index){
	assert(v != NULL);
	assert(index < v->size);
	for (int i = index;i < (v->size-1); i++){
	memcpy((char*)v->arr +i * v->elemSize,(char*)v->arr + (i+1)*v->elemSize,v->elemSize);
}
}
